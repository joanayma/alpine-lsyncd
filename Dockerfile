FROM alpine:3.5

LABEL maintainer="Joan Aymà <joan.ayma@gmail.com>"
LABEL name="registry.gitlab.com/joanayma/lsyncd:latest"
LABEL description=""

RUN apk add --no-cache lsyncd bash supervisor py2-pip && \
    pip install j2cli && mkdir -p /etc/supervisor/conf.d
ADD files/ /root/
RUN mv /root/supervisord.conf /etc/supervisor/supervisord.conf && \
    mv /root/rsync/rsyncd.conf /etc/rsyncd.conf && \
    chmod +x /root/kill-supervisor.py && chmod +x /root/entrypoint.sh

ENTRYPOINT ["/root/entrypoint.sh"]
