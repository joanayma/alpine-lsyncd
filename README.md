lsync-based container
=====================

Description
-----------

This container amends to do syncing dinamic and secure syncing between a group of deployed containers.
Features:
 - Syncing from master to slaves.
 - Syncing from masters to slaves.
 - Syncing all to all nodes.
 - Read nodes from a gcloud instance group.

Variables
---------
Almost all variables can se serialized splitted with `:`.

 `watchdirs`: Which paths needs to be synced between containers/nodes.
 `backend`: if we want to use a backend to dinamic provision nodes list.
 `nodes`: provisioned list of hostnames that will masters (or all) sync to.
 `masters` (optional): If defined, only this containers will sync to the other ones.
 hostname should be the host hostname.
 `delay`: Time that slaves will queue (delay) files from master before writing them.

Backend
-------

If backend is defined, `updater` will use it to fulfill `nodes` list.
Actually only gcloud backend is available.

If the returned list of nodes from backend changes then config and service is reloaded.

===gcloud:

use `gcloud_ig` variable to define which intance group will be monitored to return a nodes/hosts list from that instance group.


Example
-------

```
version: 3
services:
  lsyncd:
    image: registry.gitlab.com/joanayma/alpine-lsyncd:latest
    hostname: ${hostname}
    ports:
      - 587
    environment:
      - backend=gcloud
      - gcloud_ig=front-autoscaler-slaves
      - masters=front-master
      - watchdirs=/srv/web/httpdocs:/srv/web2/httpdocs
    volumes:
      - /srv/web/httpdocs
      - /srv/web2/httpdocs
