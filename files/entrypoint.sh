#!/bin/bash

# fix alpine ndots
sed '/ndots/d' /etc/resolv.conf > /tmp/resolv.conf
cat /tmp/resolv.conf > /etc/resolv.conf

echo "[entrypoint] Check mandatory variables"
if [[ ! -n "${watchdirs}" ]]; then
        echo "[entrypoint] Error: No dir(s) to monitor defined"
        exit 1
fi
if [[ (! -n "${nodes}") || ( -n "${lsyncd_backend}") ]]; then
       echo "[entrypoint] Error: No nodes especified or no backend defined"
       exit 1
else
   if [[ (! $nodes == *"$HOSTNAME"*) || ( -n "${lsyncd_backend}" ]]; then
       echo "[entrypoint] Warning: The node is not listed in nodes or no backend defined"
   fi
fi

echo "[entrypoint] Recreate rsync secret"
if [[ -n "${rsync_secret}" ]]; then
	echo "sync-user:${rsync_secret}" > /dev/shm/.rsyncd.secrets
        echo "${rsync_secret}" > /dev/shm/.rsync.secret
        chmod 600 /dev/shm/.rsyncd.secrets /dev/shm/.rsync.secret
else
	echo "[entrypoint] Error: No secret especified"
	exit 1
fi

echo "[entrypoint] Generate config from templates"
#j2 /root/jinja2.d/supervisor.d/lsyncd.conf.j2 > /etc/supervisor/conf.d/lsyncd.conf
j2 /root/jinja2.d/supervisor.d/rsyncd.conf.j2 > /etc/supervisor/conf.d/rsyncd.conf
j2 /root/jinja2.d/supervisor.d/updater.conf.j2 > /etc/supervisor/conf.d/updater.conf
j2 /root/jinja2.d/lsyncd.conf.j2 > /etc/lsyncd.conf
python /root/updater/main.py --oneshot

# supervisord
if [ -z "$@" ]; then
  echo "[entrypoint] Start supervisor"
  trap 'kill -SIGHUP `cat /tmp/supervisord.pid`' SIGUSR1
  trap 'kill -SIGTERM `cat /tmp/supervisord.pid`' SIGTERM
  exec supervisord -n -c /etc/supervisor/supervisord.conf
else
  exec $@
fi
