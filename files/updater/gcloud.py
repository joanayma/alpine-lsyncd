# gcloud backend

def get:
    # return list of instances.
    instances = []
    # Remove on prod
    from pprint import pprint
    # TODO: Import only needed methods
    import os, json
    from googleapiclient import discovery
    from oauth2client.client import GoogleCredentials
    credentials = GoogleCredentials.get_application_default()
    
    service = discovery.build('compute', 'beta', credentials=credentials)
    
    # gcloud project
    # TODO: Get actual working project by default.
    try:
        project = os.environ.get('gcloud_project')
    except:
        print("ERROR: project ennvar for gcloud project is not set")
        return 1
    
    # gcloud instance group -- instance group name where all instances will sync
    # In case of a master, config should config master with slave.
    # Slaves should not have any instance destination.
    try:
        instance_group = os.environ.get('gcloud_ig')
    except:
        print("ERROR: gcloud_ig envvar is not set")
        return 1
    
    # gcloud zone -- defaults to europe-west1-d
    zone = os.environ.get('gcloud_zone','europe-west1-d')
    
    instance_groups_list_instances_request_body = {
        # TODO: Add desired entries to the request body.
    }
    
    request = service.instanceGroups().listInstances(project=project, zone=zone, instanceGroup=instance_group, body=instance_groups_list_instances_request_body)
    while request is not None:
        response = request.execute()
    
        for instance_with_named_ports in response['items']:
            pprint(instance_with_named_ports)
            i = json.loads(json.dumps(instance_with_named_ports))
            instance_name = i['instance'].split("/")[-1:]
            instances.append(instance_name[0])
    
        request = service.instanceGroups().listInstances_next(previous_request=request, previous_response=response)
    
    print("[updater-gcloud] Returned instances: " + ''.join(instances))
    return instances
