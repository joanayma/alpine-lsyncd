from jinja2 import Environment, FileSystemLoader

from . import supervisord
from socket import gethostname
from time import sleep

global last_config, supervisord_client

supervisord_client = supervisord.SupervisorClient

def sha256_config():
    f = open('/etc/lsyncd.conf', 'rb')
    return '%s *%s' % (sha256(f.read()).hexdigest(), fn)

def sha256_compare_config(last_sha256):
    now_sha256 = sha256_config()
    if now_sha256 in last_sha256:
        print("[updater] config not modified")
        return 0
    else:
        print("[updater] config modified")
        return 1

def reload_config(supervisord_client):
    gen_lsyncd_conf("/etc/lsyncd.conf")
    supervisord_client.stop("lsyncd")
    supervisord_client.start("lsyncd")

def main(last_config, supervisord_client):
    if sha256_compare_config(last_config):
        reload_config(supervisord_client)
    last_config = sha256_config()
    print("[updater] Updated config:")
    print(gen_lsyncd_conf())
    return True

def wait_loop(seconds):
    # TODO: ensure sleep envvar is same as lsyncd sleep
    sleep(os.environ.get('sleep', '10')

def get_nodes():
    backend = os.environ.get('lsyncd_backend')
    if backend == 'gcloud':
        from . import gcloud
        return gcloud.get()
    if backend == 'aws':
        print("[updater] aws not implemented")
        exit 1

def gen_lsyncd_conf(file=null, watchdirs_list=os.environ.get('watchdirs_list'), master=os.environ.get('master', null), nodes):
    # Create the jinja2 environment.
    # Notice the use of trim_blocks, which greatly helps control whitespace.
    j2_env = Environment(loader=FileSystemLoader('../jinja2.d'),
                         trim_blocks=True)
    conf = j2_env.get_template('lsyncd.conf.j2').render(
        watchdirs_list=watchdirs_list, master=master, nodes=nodes, hostname=gethostname())
    )
    if file is not null:
        with open(file, 'wb') as fh:
            fh.write(conf)
    else:
        return conf

if __name__ == '__main__':
    
    err_code = main()
    if len(sys.argv) > 1:
        if 'oneshot' in sys.argv[1]:
            reload_config(last_config, supervisord_client)
            exit 0
    while True in err_code:
        err_code = main()
        wait_loop()

